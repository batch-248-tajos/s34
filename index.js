//express package was imported as express
const express = require("express");

//invoked express package to create a server/api and save it in a variable which we can refert to later to create routes
const app = express();

//variable for port assignment
const port = 4000;

//express.json() is a method from express that allow us to handle the stream of data from our client and receive the data and automatically parse the incoming JSON from our request

//app.use() is a method used to run another function or method for our expressjs api
//it is used to run middlewares

app.use(express.json())

//by applying the option of "extended:true" this allows us to receive information in other data types such as an object whcih we will use throughout our application
app.use(express.urlencoded({extended:true}))

//CREATE A ROUTE IN EXPRESS
	//express has methods corresponding to each HTTP Method


app.get("/",(req,res)=>{

	//res.send uses the express JS Module's method instead to send a response back to the client
	res.send("Hello from our default ExpressJs GET route!")
})

app.post("/",(req,res)=>{

	//res.send("Hello from our default ExpressJs POST route!")
	res.send(`Hello ${req.body.firstName} ${req.body.lastName}`)
})

//MA1
	//Refactor the post route to receive data from the req.body
	//Send a message that has the data received
//MA2
	//Create a route for a put method request on the "/" endpoint
	//Send a message as a response: "Hello from the ExpressJS PUT method route!"

	//Create a route for a delete method request on the "/" endpoint
	//Send a message as a response: "Hello from the ExpressJS DELETE method route!"

			//{
			//    "firstName": "Cardo",
			//    "lastName": "Dalisay"
			//}

app.put('/',(req,res)=>{

	res.send("Hello from a put method route!")

})

app.delete('/',(req,res)=>{

	res.send("Hello from a delete method route!")

})

//Add a Mock Database

let users = [

		{
			username: "cardo_dalisay",
			password:"quiapo"	
		},
		{
			username: "mommy_d",
			password:"nardadarna"	
		},
		{
			username: "kagawad_godbless",
			password:"dingangbato"	
		}
	]


app.post('/signup',(req,res)=>{

	//request/req.body contains the body of the request or the input
	users.push(req.body)
	res.send(users)
})

//MA3

//Refactor the signup route
	//If contents of the "request body" with the property "username" and "password" is not empty, this will store the user object sent via Postman to the users array created above
	//This will send a response back to the client/Postman after the request has been processed

	//Else the username and password are not complete an error message will be sent back to the client/Postman

	
	



//tells our server to listen to the port
app.listen(port,()=> console.log(`Server running at port ${port}`))
