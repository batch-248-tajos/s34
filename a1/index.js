const express = require("express")

const app = express()
const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended: true}))

//Mock Database

let users = [

    {
        username: "cardo_dalisay",
        password:"quiapo"	
    },
    {
        username: "mommy_d",
        password:"nardadarna"	
    },
    {
        username: "kagawad_godbless",
        password:"dingangbato"	
    }
]

app.get('/home', (req, res) => {
    res.send('Welcome to the homepage')
})

app.get('/items', (req, res) => {
    res.send(users)
})

app.delete('/delete-item', (req, res) => {

    if (Object.keys(req.body).includes('username')) {
        let deleted
        const username = req.body.username

        users = users.filter((element) => {
            const isFound = element.username === username

            if (deleted === undefined || false)
                deleted = isFound

            return !isFound
        })

        if (deleted)
            res.send(`User ${username} has been deleted.`)
        else {
            // res.send(users)
            res.send(`User ${username} does not exist!`)
        }

    } else {
        res.send('Unable to process your request!')
    }
})

app.listen(port, () => {
    console.log(`Server running at localhost:${port}`)
})